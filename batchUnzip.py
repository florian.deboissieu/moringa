import getopt
import os
import sys
import glob
import zipfile


def unzip(argv):
    try:
        opts, args = getopt.getopt(argv, 'ro:')
    except getopt.GetoptError as err:
        print str(err)

    od = args[0]
    removeZip = False

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-r':
            removeZip = True

    #Unzip
    zipFiles = glob.glob(args[0]+os.sep+'*.zip')
    for zf in zipFiles:
        zfile = zipfile.ZipFile(zf, 'r')
        for i in zfile.namelist():
            if os.path.isdir(i) or i[-1] == '/':
                try :
                    os.makedirs(od+os.sep+i)
                except:
                    pass
            else :
                try: os.makedirs(od + os.sep + os.path.dirname(i))
                except: pass
                file = zfile.read(i)
                fp = open(od+os.sep+i, "w+b")
                fp.write(file)
                fp.close()
        zfile.close()
        if removeZip:
            os.remove(zf)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python batchUnzip.py [-o <output folder - def. zip folder>] [-r] <folder containing zip files>\n'
            'Searches for all available zip file and unzip them all.')
    else:
        zipFiles = glob.glob(sys.argv[-1] + '/*.zip')
        if len(zipFiles) > 0:
            unzip(sys.argv[1:])
        else:
            sys.exit(
                'No zip file found in the given folder\n'
                'Usage: python batchUnzip.py [-o <output folder - def. zip folder>] [-r] <folder containing zip files>\n')