#! /usr/bin/env python
import sys
import os
import getopt
import ogr
import osr
import platform
import subprocess
from datetime import date

from mtdUtils import getShapefileExtent_WGS84

def main(argv):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    try:
        opts, args = getopt.getopt(argv, 'w:d:f:c:p:m')
    except getopt.GetoptError as err:
        sys.exit(err)

    shp_file = args[-2]
    print shp_file
    if not os.path.exists(shp_file):
        sys.exit('%s does not exist'%shp_file)
    peps_folder = args[-1]
    if not os.path.exists(peps_folder):
        sys.exit('%s does not exist, please give the path to the peps_download folder'%peps_folder)
    else :
        id_file = peps_folder + os.sep + 'peps.txt'
        if not os.path.exists(id_file) :
            sys.exit('%s does not exist, the usual file for peps ID is named "peps.txt"'%id_file)
    output_folder = os.path.dirname(shp_file)
    date_beg = None
    date_end = None
    collection = 'S2ST'
    product_type = 'S2MSI1C'
    sensor_mode = None

    for opt,val in opts:
        if opt == '-w':
            output_folder = str(val)
            if not os.path.exists(output_folder):
                os.mkdir(output_folder)
        elif opt == '-d':
            date_beg = str(val)
        elif opt == '-f':
            date_end = str(val)
        elif opt == '-c' and val in ['S1','S2','S2ST','S3']:
            collection = str(val)
        elif opt == '-p':
            product_type = str(val)
        elif opt == '-m':
            sensor_mode = str(val)
    if collection == 'S2ST' :
        if date_beg == None :
            date_beg = '2016-12-06'
        if date_end == None :
            date_end = date.today().isoformat()
    if collection == 'S2' :
        if date_end == None :
            date_end = '2016-12-06'
        if date_beg == None :
            date_beg = date.today().isoformat()

    extent = getShapefileExtent_WGS84(shp_file)

    cmd = ['python', peps_folder+'/peps_download.py', '-a', id_file, '-w', output_folder, '-c', collection, '-p', product_type,
    	'--lonmin', str(extent[0]), '--lonmax',  str(extent[2]), '--latmin', str(extent[3]), '--latmax', str(extent[1])]
    if date_beg != None :
        cmd += ['-d', date_beg]
    if date_end != None:
        cmd += ['-f', date_end]
    if sensor_mode != None:
        cmd += ['-m', sensor_mode]
    if sh == True :
        cmd += ['--windows']
    print cmd
    subprocess.call(cmd, shell=sh)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: python getS2PEPSFromShp.py [-w <output folder>] [-d <begin-date YYYY-MM-DD>] [-f <end-date YYYY-MM-DD>] [-c <collection>] [-p <product-type>] [-m <sensor-mode>] <shp-file> <peps-installation-folder>')
    else:
        main(sys.argv[1:])