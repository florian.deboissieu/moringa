import getopt
import json
import os
import subprocess
import sys
import xml.etree.ElementTree as ET

from mtdUtils import bufferedVectorDifference, randomword, getRasterExtentAsShapefile


def getSentinel2_AmazonAWS(date, tile, od, seq=0):
    # Create output directory
    os.mkdir(od)
    # Parse date "yyyymmdd"
    year = date[0:4]
    month = date[4:6]
    day = date[6:8]
    # Parse tile ex. "38KPV"
    mrd = tile[0:2]
    tl1 = tile[2]
    tl2 = tile[3:5]
    # create base WGET command
    page = '/'.join([mrd, tl1, tl2, str(int(year)), str(int(month)), str(int(day)), str(seq)])
    baseurl = 'http://sentinel-s2-l1c.s3-website.eu-central-1.amazonaws.com/tiles/' + page + '/'
    # Initialize and fill file list
    url_list = randomword(8) + '.txt'
    f = open(url_list, 'w')
    fns = ['B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B09', 'B10', 'B11', 'B12']
    out_fns = []
    for fn in fns:
        f.write(baseurl + fn + '.jp2' + '\n')
        out_fns.append(od + '/' + fn + '.jp2')
    f.write(baseurl + 'metadata.xml' + '\n')
    f.write(baseurl + 'tileInfo.json' + '\n')
    f.write(baseurl + 'qi/MSK_CLOUDS_B00.gml' + '\n')
    msk_fn = od + '/MSK_CLOUDS_B00.gml'
    f.close()
    # call WGET
    subprocess.call('wget -q -i ' + url_list + ' -P \"' + od + '\"', shell=True)
    os.remove(url_list)
    return out_fns, msk_fn


def readS2Metadata(mdf):
    # Init xml tree
    tree = ET.parse(mdf)
    root = tree.getroot()
    # Prepare output
    out = {}
    # For each info, prepare iterator, read and store
    # 1 - CLOUDY_PIXEL_PERCENTAGE
    it = root.iter('CLOUDY_PIXEL_PERCENTAGE')
    info = it.next()
    out[info.tag] = float(info.text)
    # Return dictionary
    return out


def readS2TileInfo(tli):
    with open(tli) as json_data:
        data = json.load(json_data)
    out = {}
    # For each info, read and store as dictionary (casting format)
    # 1 - DATA COVERAGE PERCENTAGE
    out['dataCoveragePercentage'] = float(data['dataCoveragePercentage'])
    # 2 - CLOUDY PIXEL PERCENTAGE
    out['cloudyPixelPercentage'] = float(data['cloudyPixelPercentage'])
    return out


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:rm:',['sequence='])
    except getopt.GetoptError as err:
        print str(err)

        # Parse options
    od = os.getcwd()
    clipping = False
    clip_shp = None
    removeOriginal = False
    maskClouds = False
    buf = 0
    seq = 0

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
            clipping = True
        elif opt == '-r':
            removeOriginal = True
        elif opt == '-m':
            maskClouds = True
            buf = float(val)
        elif opt == '--sequence':
            seq = int(val)

    if os.environ.get('GDAL_DATA') == None:
        sys.exit('Please set GDAL_DATA environment variable!')

    # Create output directories
    if not os.path.exists(od):
        os.mkdir(od)
    od = od + '/S2_' + args[0] + '_' + args[1]
    if os.path.exists(od):
        print('Output directory already exists!')
    else:
        # DOWNLOAD COMMAND
        print('Downloading ' + od + '...')
        out_fns, msk_fn = getSentinel2_AmazonAWS(args[0], args[1], od, seq)

        # Check if clouds exist (in tile info)
    mtd = readS2TileInfo(od + '/tileInfo.json')
    cld = (mtd['cloudyPixelPercentage'] > 0)
    maskClouds &= cld

    curdir = os.getcwd()
    os.chdir(od)

    print('Processing...')
    # Prepare ROI (full extent if not clipping)
    tmpname = randomword(16)
    if not clipping:
        clip_shp = od + '/' + tmpname + '.shp'
        getRasterExtentAsShapefile(od + '/B01.jp2', clip_shp)
        ext = 'full'
    else:
        ext = os.path.splitext(os.path.basename(clip_shp))[0]
        # Handle cloud mask (if cloudy)
    if cld:
        msk_fn = od + '/MSK_CLOUDS_B00' + '_' + ext + '.shp'
        clip_cmd = 'ogr2ogr -clipsrc \"' + clip_shp + '\" \"' + msk_fn + '\" \"' + od + '/MSK_CLOUDS_B00.gml\"'
        subprocess.call(clip_cmd, shell=True)
    if maskClouds:
        clip_shp_val = od + "/" + ext + '_noclouds.shp'
        bufferedVectorDifference(clip_shp, msk_fn, clip_shp_val, buf)
    else:
        clip_shp_val = clip_shp
        # Generate and run clip commands
    clip_cmd = 'gdalwarp -q -of GTiff -ot UInt16 -dstnodata 0 -srcnodata 0 -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
    fns = ['B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B09', 'B10', 'B11', 'B12']
    out_fns = []
    for fn in fns:
        out_fn = od + '/' + fn + '_' + ext + '.tif'
        out_fns.append(out_fn)
        subprocess.call(clip_cmd + '\"' + od + '/' + fn + '.jp2\" \"' + out_fn + '\"', shell=True)
        # Clean temporary files and original bands if requested
    files = os.listdir(os.getcwd())
    if maskClouds:
        for file in files:
            if "_noclouds" in file or tmpname in file:
                os.remove(os.path.join(os.getcwd(), file))
    if removeOriginal:
        for file in files:
            if file.endswith(".jp2") or file.endswith(".gml") or file.endswith(".gfs"):
                os.remove(os.path.join(os.getcwd(), file))

    os.chdir(curdir)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: python getSentinel2.py [-o <output dir>] [-c <clip_shp>] [-r] [-m <bufsize>] <date "yyyymmdd"> <tile nnXYY>')
    else:
        main(sys.argv[1:])
