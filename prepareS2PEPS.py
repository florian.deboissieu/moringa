import getopt
import os
import subprocess
import sys
import glob
import shutil
import platform
import stat
import zipfile

from mtdUtils import randomword, getRasterExtentAsShapefile

def genScript(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:t:')
    except getopt.GetoptError as err:
        print str(err)

    # Get sub-folders names
    dirs = [args[0] + os.sep + dn for dn in os.listdir(args[0]) if os.path.isdir(args[0] + os.sep + dn)]

    # Parse options
    od = os.getcwd()
    clip_shp = None
    single_tile = ''

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
        elif opt == '-t':
            single_tile = val

    if platform.system() == 'Windows':
        f = open('S2PEPSPreparation.bat', 'w')
        f.write("@echo off\n")
    else:
        f = open('S2PEPSPreparation.sh', 'w')

    for dir in dirs:
        if single_tile != '' and single_tile not in dir:
            continue
        else:
            mtd = glob.glob(dir + '/*MTD_MSIL1C.xml')
            if len(mtd) > 0:
                cmd = ['python', 'prepareS2PEPS.py', '-o', '\"' + od + '\"']
                if clip_shp is not None:
                    cmd += ['-c', '\"' + clip_shp + '\"']
                cmd += ['\"' + dir + '\"']
                f.write(' '.join(cmd) + '\n')
    f.close()

    if platform.system() == 'Linux':
        st = os.stat('S2PEPSPreparation.sh')
        os.chmod('S2PEPSPreparation.sh', st.st_mode | stat.S_IEXEC)

    return

def prepare(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:')
    except getopt.GetoptError as err:
        print str(err)

    # Parse options
    od = os.getcwd()
    clipping = False
    clip_shp = None

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
            clipping = True

    src_dir = args[0]
    tile_date = os.path.basename(src_dir)[11:19]
    tile_id = os.path.basename(src_dir)[39:44]

    if os.environ.get('GDAL_DATA') == None:
        sys.exit('Please set GDAL_DATA environment variable!')

    print('Processing ' + src_dir + '...')

    # Create output directories
    if not os.path.exists(od):
        os.mkdir(od)
    seqnum = 0
    pd = od
    od = pd + '/S2-L1C-PEPS_' + str(seqnum) + '_' + tile_date + '_' + tile_id
    while os.path.exists(od):
        seqnum += 1
        od = pd + '/S2-L1C-PEPS_' + str(seqnum) + '_' + tile_date + '_' + tile_id
    os.mkdir(od)

    curdir = os.getcwd()
    os.chdir(od)

    with open(od + '/tileInfo.json', 'wb') as mf:
        mf.write('*** Dummy metadata file for S2-L1C-PEPS_' + str(seqnum) + '_' + tile_date + '_' + tile_id + ' ***')

    # Prepare ROI (full extent if not clipping)
    tmpname = randomword(16)
    if not clipping:
        clip_shp = od + '/' + tmpname + '.shp'
        ext_ref = glob.glob(src_dir + '/GRANULE/*/IMG_DATA/*_B02.jp2')[0]
        getRasterExtentAsShapefile(ext_ref, clip_shp)
        ext = 'full'
    else:
        ext = os.path.splitext(os.path.basename(clip_shp))[0]

    clip_shp_val = clip_shp
    # Generate and run clip commands
    clip_cmd = 'gdalwarp -q -of GTiff -ot Int16 -dstnodata -10000 -srcnodata 0 -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
    fns = ['B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B09', 'B10', 'B11', 'B12']

    for fn_pre in fns:
        fn = glob.glob(src_dir + '/GRANULE/*/IMG_DATA/*_' + fn_pre + '.jp2')[0]
        out_fn = od + '/' + os.path.splitext(os.path.basename(fn))[0] + '_' + ext + '.tif'
        subprocess.call(clip_cmd + '\"' + fn + '\" \"' + out_fn + '\"', shell=True)

    # clip_cmd = 'gdalwarp -q -of GTiff -ot UInt16 -dstnodata 0 -srcnodata 0 -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
    # fns = ['CLM_R1', 'CLM_R2']
    # for fn_pre in fns:
    #     fn = glob.glob(src_dir + '/MASKS/*_' + fn_pre + '.tif')[0]
    #     out_tmp_fn = od + '/' + os.path.splitext(os.path.basename(fn))[0] + '_tmp.tif'
    #     cmd = ['otbcli_BandMath', '-il', fn, '-exp', 'im1b1+1', '-out', out_tmp_fn, 'uint16']
    #     subprocess.call(' '.join(cmd), shell=True)
    #     out_fn = od + '/' + os.path.splitext(os.path.basename(fn))[0] + '_' + ext + '.tif'
    #     subprocess.call(clip_cmd + '\"' + out_tmp_fn + '\" \"' + out_fn + '\"', shell=True)
    #     os.remove(out_tmp_fn)

    mtd_fn = glob.glob(src_dir + '/GRANULE/*/MTD_TL.xml')[0]
    shutil.copyfile(mtd_fn, od + '/' + 'metadata.xml')

    os.chdir(curdir)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: python prepareS2PEPS.py [-o <output dir>] [-c <clip_shp>] [-t tile] <original PEPS tile folder / parent folder>\n'
            'If parent folder is given, searches for all available images and creates a preparation batch file.')
    else:
        mtd = glob.glob(sys.argv[-1] + '/*MTD_MSIL1C.xml')
        if len(mtd) > 0:
            prepare(sys.argv[1:])
        else:
            genScript(sys.argv[1:])