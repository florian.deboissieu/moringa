import getopt
import glob
import os
import platform
import subprocess
import sys
from mtdUtils import setNoDataValue


def getSizeFromOTBInfo(str):
    str_lines = str.split('\n')
    line = None
    W, H = -1, -1
    for x in str_lines:
        if 'Size :' in x:
            line = x
    if line is not None:
        line = line[line.index('[') + 1:line.index(']')].split(',')
        W, H = int(line[0]), int(line[1])

    return W, H


def L8_preprocess(fld, cmask=False):
    if platform.system() == 'Linux':
        gdl_merge = ['gdal_merge.py', '-separate', '-of', 'GTiff', '-o']
        fmsk = ['python', 'cloudMask.py', '--HR']
        fmsk_angles = ['fmask_usgsLandsatMakeAnglesImage.py']
        fmsk_toa = ['fmask_usgsLandsatTOA.py']
        sh = False
    elif platform.system() == 'Windows':
        gdl_merge = ['gdal_merge', '-separate', '-of', 'GTiff', '-o']
        fmsk = ['python', 'cloudMask.py', '--HR']
        fmsk_angles = ['python', 'fmask_usgsLandsatMakeAnglesImage.py']
        fmsk_toa = ['python', 'fmask_usgsLandsatTOA.py']
        sh = True
    else:
        sys.exit("Platform not supported!")

    mdf = ['-m', glob.glob(fld + '/*_MTL*.txt')[0]]
    angles_fn = [fld + '/angles.tif']
    oli_toa_fn = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_OLI_TOA')]

    if cmask:
        subprocess.call(fmsk + [fld])

    else:

        b1fn = ''
        b1fns = glob.glob(fld + '/*_B1*.TIF')
        for fln in b1fns:
            if 'B10' not in fln and 'B11' not in fln:
                b1fn = fln

        oli_list = [b1fn,
                    glob.glob(fld + '/*_B2*.TIF')[0],
                    glob.glob(fld + '/*_B3*.TIF')[0],
                    glob.glob(fld + '/*_B4*.TIF')[0],
                    glob.glob(fld + '/*_B5*.TIF')[0],
                    glob.glob(fld + '/*_B6*.TIF')[0],
                    glob.glob(fld + '/*_B7*.TIF')[0],
                    glob.glob(fld + '/*_B9*.TIF')[0]]

        oli_fn = [fld + '/oli.tif']

        subprocess.call(gdl_merge + oli_fn + oli_list, shell=sh)
        subprocess.call(fmsk_angles + mdf + ['-t'] + oli_fn + ['-o'] + angles_fn, shell=sh)
        subprocess.call(fmsk_toa + ['-i'] + oli_fn + mdf + ['-z'] + angles_fn + ['-o'] + oli_toa_fn, shell=sh)

        os.remove(oli_fn[0])
        os.remove(angles_fn[0])
        for f in glob.glob(fld + '/*.aux.xml'):
            os.remove(f)

    pan_fn = [glob.glob(fld + '/*_B8*.TIF')[0]]
    pan_toa_fn = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_PAN_TOA')]
    ps_toa_fn = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_PS_TOA')]
    subprocess.call(fmsk_angles + mdf + ['-t'] + pan_fn + ['-o'] + angles_fn, shell=sh)
    subprocess.call(fmsk_toa + ['-i'] + pan_fn + mdf + ['-z'] + angles_fn + ['-o'] + pan_toa_fn, shell=sh)

    subprocess.call(
        ['otbcli_Superimpose', '-inr'] + pan_toa_fn + ['-inm'] + oli_toa_fn + ['-out', fld + '/ms.tif'],
        shell=sh)
    subprocess.call(
        ['otbcli_Pansharpening', '-inp'] + pan_toa_fn + ['-inxs', fld + '/ms.tif', '-out', fld + '/ps.tif', 'int16',
                                                         '-method',
                                                         'bayes'],
        shell=sh)
    subprocess.call(
        ['otbcli_ManageNoData', '-in'] + pan_fn + ['-mode', 'buildmask', '-out', fld + '/msk.tif', 'uint8'],
        shell=sh)
    subprocess.call(
        ['otbcli_ManageNoData', '-in', fld + '/ps.tif', '-mode', 'apply', '-mode.apply.mask', fld + '/msk.tif',
         '-out'] + ps_toa_fn + ['uint16'],
        shell=sh)
    # subprocess.call(
    #    ['otbcli_BandMathX', '-il', fld + '/ps.tif', '-exp', 'im1', '-out'] + ps_toa_fn + ['uint16'],
    #    shell=sh)

    os.remove(angles_fn[0])
    os.remove(fld + '/ms.tif')
    os.remove(fld + '/ps.tif')
    os.remove(fld + '/msk.tif')
    for f in glob.glob(fld + '/*.aux.xml'):
        os.remove(f)

    if cmask:
        msk_fn = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_MASK_HR')]
        gmsk_fn = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_GAPMASK')]
        # ps_toa_msk_fn_tmp = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_PS_TOA_MASKED_TMP')]
        # ps_toa_msk_fn = [glob.glob(fld + '/*_B2*.TIF')[0].replace('_B2', '_PS_TOA_MASKED')]
        # subprocess.call(
        #    ['otbcli_ManageNoData', '-in'] + ps_toa_fn + ['-mode', 'apply', '-mode.apply.mask'] + msk_fn + [
        #        '-out'] + ps_toa_msk_fn_tmp + ['uint16'],
        #    shell=sh)
        # subprocess.call(
        #    ['otbcli_ManageNoData', '-in'] + ps_toa_msk_fn_tmp + ['-mode', 'changevalue', '-mode.changevalue.newv', '0',
        #                                                          '-out'] + ps_toa_msk_fn + ['uint16'],
        #    shell=sh)
        subprocess.call(
            ['otbcli_ManageNoData', '-in'] + pan_toa_fn + ['-mode', 'buildmask', '-out', fld + '/tmpmask.tif', 'uint8'],
            shell=sh)
        subprocess.call(
            ['otbcli_BandMathX', '-il', fld + '/tmpmask.tif'] + msk_fn + ['-exp', '{im1b1 != 0 && im2b1 == 0}',
                                                                          '-out'] + gmsk_fn + ['uint8'],
            shell=sh)
        # os.remove(ps_toa_msk_fn_tmp[0])
        os.remove(fld + '/tmpmask.tif')


def S2_preprocess(fld, cmask=False, ref=None,
                  bandlist=None):
    if platform.system() == 'Linux':
        fmsk = ['python', 'cloudMask.py', '--HR']
        sh = False
    elif platform.system() == 'Windows':
        fmsk = ['python', 'cloudMask.py', '--HR']
        sh = True
    else:
        sys.exit("Platform not supported!")

    if ref is None:
        ref = 'B02'
    if bandlist is None:
        bandlist = ['B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B11', 'B12']

    if cmask:
        subprocess.call(fmsk + [fld])

    bnd_list = []
    for b in bandlist:
        bnd_list.append(glob.glob(fld + '/*' + b + '*.tif')[0])

    bnd_ref = [glob.glob(fld + '/*' + ref + '*.tif')[0]]

    bnd_tomerge = []
    ref_info = subprocess.check_output(['otbcli_ReadImageInfo', '-in'] + bnd_ref, shell=sh)
    W, H = getSizeFromOTBInfo(ref_info)
    for bf in bnd_list:
        img_info = subprocess.check_output(['otbcli_ReadImageInfo', '-in', bf], shell=sh)
        w, h = getSizeFromOTBInfo(img_info)
        if w != W or h != H:
            bnd_tomerge.append(bf.replace('.tif', '_ref.tif'))
            subprocess.call(['otbcli_Superimpose', '-inr'] + bnd_ref + ['-inm', bf, '-out', bnd_tomerge[-1], 'uint16'],
                            shell=sh)
        else:
            bnd_tomerge.append(bf)

    stack_fn = [glob.glob(fld + '/*B02*.tif')[0].replace('B02', 'STACK')]
    subprocess.call(['otbcli_ConcatenateImages', '-il'] + bnd_tomerge + ['-out'] + stack_fn + ['uint16'], shell=sh)

    for bf in bnd_tomerge:
        if '_ref' in bf:
            os.remove(bf)

    if cmask:
        msk_fn = [glob.glob(fld + '/*B02*.tif')[0].replace('B02', 'MASK')]
        gmsk_fn = [glob.glob(fld + '/*B02*.tif')[0].replace('B02', 'GAPMASK')]
        # stack_msk_fn = [glob.glob(fld + '/*B02*.tif')[0].replace('B02', 'STACK_MASKED')]
        # subprocess.call(
        #    ['otbcli_ManageNoData', '-in'] + stack_fn + ['-mode', 'apply', '-mode.apply.mask'] + msk_fn + [
        #        '-out'] + stack_msk_fn + ['uint16'],
        #    shell=sh)
        subprocess.call(
            ['otbcli_ManageNoData', '-in'] + bnd_ref + ['-mode', 'buildmask', '-out', fld + '/tmpmask.tif', 'uint8'],
            shell=sh)
        subprocess.call(
            ['otbcli_BandMathX', '-il', fld + '/tmpmask.tif'] + msk_fn + ['-exp', '{im1b1 != 0 && im2b1 == 0}',
                                                                          '-out'] + gmsk_fn + ['uint8'],
            shell=sh)
        os.remove(fld + '/tmpmask.tif')

    return

def S2_THEIA_preprocess(fld, cmask=False, ref=None,
                  bandlist=None):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    if ref is None:
        ref = 'B2'
    if bandlist is None:
        bandlist = ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']

    if cmask:
        msk_fn = glob.glob(fld + '/*_FRE_B2*.tif')[0].replace('B2', 'MASK')
        if ref in ['B2', 'B3', 'B4', 'B8A']:
            msk_res = 'R1'
        else:
            msk_res = 'R2'
        cl_msk_fn = glob.glob(fld + '/*_CLM_' + msk_res + '*.tif')[0]
        cmd = ['otbcli_BandMathX', '-il', cl_msk_fn, '-out', msk_fn, 'uint8', '-exp', '{im1b1 == 1}']
        subprocess.call(cmd, shell=sh)

    bnd_list = []
    for b in bandlist:
        vfns = glob.glob(fld + '/*_FRE_' + b + '*.tif')
        if b == 'B8':
            for f in vfns:
                if 'B8A' not in f:
                    vfn = f
        else:
            vfn = vfns[0]
        bnd_list.append(vfn)

    bnd_ref = [glob.glob(fld + '/*_FRE_' + ref + '*.tif')[0]]

    bnd_tomerge = []
    ref_info = subprocess.check_output(['otbcli_ReadImageInfo', '-in'] + bnd_ref, shell=sh)
    W, H = getSizeFromOTBInfo(ref_info)
    for bf in bnd_list:
        img_info = subprocess.check_output(['otbcli_ReadImageInfo', '-in', bf], shell=sh)
        w, h = getSizeFromOTBInfo(img_info)
        if w != W or h != H:
            bnd_tomerge.append(bf.replace('.tif', '_ref.tif'))
            subprocess.call(['otbcli_Superimpose', '-inr'] + bnd_ref + ['-inm', bf, '-out', bnd_tomerge[-1], 'int16'], shell=sh)
        else:
            bnd_tomerge.append(bf)

    stack_fn = [glob.glob(fld + '/*_FRE_B2*.tif')[0].replace('B2', 'STACK')]

    # new
    expr = ['(int)(im' + str(k+1) + 'b1 != -10000) * (im' + str(k+1) + 'b1 + 1)' for k in range(len(bnd_tomerge))]
    expr = '{' + ';'.join(expr) + '}'
    cmd = ['otbcli_BandMathX','-il'] + bnd_tomerge + ['-out'] + stack_fn + ['uint16', '-exp', expr]
    subprocess.call(cmd, shell=sh)
    setNoDataValue(stack_fn[0], 0)

    # old
    #subprocess.call(['otbcli_ConcatenateImages', '-il'] + bnd_tomerge + ['-out'] + stack_fn + ['int16'], shell=sh)

    for bf in bnd_tomerge:
        if '_ref' in bf:
            os.remove(bf)

    if cmask:
        gmsk_fn = glob.glob(fld + '/*_FRE_B2*.tif')[0].replace('B2', 'GAPMASK')

        subprocess.call(
            ['otbcli_ManageNoData', '-in'] + bnd_ref + ['-mode', 'buildmask', '-out', fld + '/tmpmask.tif', 'uint8'],
            shell=sh)
        subprocess.call(
            ['otbcli_BandMathX', '-il', fld + '/tmpmask.tif', msk_fn, '-exp', '{im1b1 != 0 && im2b1 == 0}', '-out', gmsk_fn , 'uint8'],
            shell=sh)
        os.remove(fld + '/tmpmask.tif')

    return

def main(argv):
    try:
        opts, args = getopt.getopt(argv, '', ['cloudmask', 's2ref=', 's2bandlist='])
    except getopt.GetoptError as err:
        print str(err)

    cmask = None
    s2_ref = None
    s2_bandlist = None

    fld = args[0]
    for opt, val in opts:
        if opt == '--cloudmask':
            cmask = True
        elif opt == '--s2ref':
            s2_ref = val
        elif opt == '--s2bandlist':
            s2_bandlist = val.split(':')

    # check if S2, S2 THEIA or L8 (search for metadata file)
    mdfL8 = glob.glob(fld + '/*_MTL*.txt')
    mdfS2 = glob.glob(fld + '/tileInfo.json')
    mdfS2THEIA = glob.glob(fld + '/*_MTD_ALL.xml')
    if len(mdfL8) > 0:
        L8_preprocess(fld, cmask)
    elif len(mdfS2) > 0:
        S2_preprocess(fld, cmask, s2_ref, s2_bandlist)
    elif len(mdfS2THEIA) > 0:
        S2_THEIA_preprocess(fld, cmask, s2_ref, s2_bandlist)
    else:
        sys.exit("No supported Landsat 8 or Sentinel-2 data in " + fld)
    return 0


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python preprocess.py [--cloudmask] [--s2ref <ref_band>] [--s2bandlist <colon separated band list>] <image dir>')
    else:
        main(sys.argv[1:])
