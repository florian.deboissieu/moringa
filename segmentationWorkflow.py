import os
import platform
import subprocess
import sys
import gdal
import ogr
import glob
import shutil
import warnings

from mtdUtils import buffer, rasterizeOnReference, checkSRS
from mrzonalstats import generateGridBasedSubsets

def createOGRFromReference(src_ds,fn):
    shpd = ogr.GetDriverByName('ESRI Shapefile')
    dst = shpd.CreateDataSource(fn)
    ly = src_ds.GetLayer()
    dst_ly = dst.CreateLayer(os.path.splitext(os.path.basename(fn))[0],
                             srs=ly.GetSpatialRef(),
                             geom_type=ly.GetLayerDefn().GetGeomType())
    feat = ly.GetFeature(0)
    [dst_ly.CreateField(feat.GetFieldDefnRef(i)) for i in range(feat.GetFieldCount())]

    return dst

def intersectSamples(src,intsec,out,splitting=False,area_th=0):
    shpd = ogr.GetDriverByName('ESRI Shapefile')
    src_ds = shpd.Open(src,0)
    intsec_ds = shpd.Open(intsec, 0)

    if os.path.isfile(out):
        shpd.DeleteDataSource(out)

    out_ds = createOGRFromReference(src_ds,out)

    src_ly = src_ds.GetLayer()
    intsec_ly = intsec_ds.GetLayer()
    dst_ly = out_ds.GetLayer()

    f = intsec_ly.GetFeature(0)
    [dst_ly.CreateField(f.GetFieldDefnRef(i)) for i in range(f.GetFieldCount())]

    extent = intsec_ly.GetExtent()
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(extent[0], extent[2])
    ring.AddPoint(extent[1], extent[2])
    ring.AddPoint(extent[1], extent[3])
    ring.AddPoint(extent[0], extent[3])
    ring.AddPoint(extent[0], extent[2])
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    src_ly.SetSpatialFilter(poly)

    for feat1 in src_ly:
        geom1 = feat1.GetGeometryRef()
        intsec_ly.SetSpatialFilter(geom1)
        for feat2 in intsec_ly:
            geom2 = feat2.GetGeometryRef()
            if geom2.Intersects(geom1):
                intsec_geom = geom2.Intersection(geom1)
                if intsec_geom is not None:
                    gg = []
                    if splitting is True and intsec_geom.GetGeometryName() == "MULTIPOLYGON":
                        for geom_part in intsec_geom:
                            gg.append(geom_part)
                    else:
                        gg.append(intsec_geom)
                    for g in gg:
                        if area_th is not None and g.GetArea() > area_th:
                            dstfeat = ogr.Feature(dst_ly.GetLayerDefn())
                            dstfeat.SetGeometry(g)
                            [dstfeat.SetField(i,feat1.GetField(i)) for i in range(feat1.GetFieldCount())]
                            [dstfeat.SetField(i+feat1.GetFieldCount(), feat2.GetField(i)) for i in range(feat2.GetFieldCount())]
                            dst_ly.CreateFeature(dstfeat)

    nft = dst_ly.GetFeatureCount()

    src_ds = None
    intsec_ds = None
    out_ds = None

    if nft == 0:
        shpd.DeleteDataSource(out)
        return False
    else:
        return True

def selectSamples(src,intsec,out):
    shpd = ogr.GetDriverByName('ESRI Shapefile')
    src_ds = shpd.Open(src,0)
    intsec_ds = shpd.Open(intsec, 0)

    if os.path.isfile(out):
        shpd.DeleteDataSource(out)

    out_ds = createOGRFromReference(src_ds,out)

    src_ly = src_ds.GetLayer()
    intsec_ly = intsec_ds.GetLayer()
    dst_ly = out_ds.GetLayer()

    f = intsec_ly.GetFeature(0)
    [dst_ly.CreateField(f.GetFieldDefnRef(i)) for i in range(f.GetFieldCount())]

    extent = intsec_ly.GetExtent()
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(extent[0], extent[2])
    ring.AddPoint(extent[1], extent[2])
    ring.AddPoint(extent[1], extent[3])
    ring.AddPoint(extent[0], extent[3])
    ring.AddPoint(extent[0], extent[2])
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    src_ly.SetSpatialFilter(poly)

    for feat1 in src_ly:
        geom1 = feat1.GetGeometryRef()
        intsec_ly.SetSpatialFilter(geom1)
        for feat2 in intsec_ly:
            geom2 = feat2.GetGeometryRef()
            if geom2.Intersects(geom1):
                intsec_geom = geom1
                dstfeat = ogr.Feature(dst_ly.GetLayerDefn())
                dstfeat.SetGeometry(intsec_geom)
                [dstfeat.SetField(i,feat1.GetField(i)) for i in range(feat1.GetFieldCount())]
                [dstfeat.SetField(i+feat1.GetFieldCount(), feat2.GetField(i)) for i in range(feat2.GetFieldCount())]
                dst_ly.CreateFeature(dstfeat)
                break

    nft = dst_ly.GetFeatureCount()

    src_ds = None
    intsec_ds = None
    out_ds = None

    if nft == 0:
        shpd.DeleteDataSource(out)
        return False
    else:
        return True


def segmentationWorkflow(seg_fld, seg_src, extent, params, seg_output):
    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
        polycmd = 'gdal_polygonize.py'
    elif platform.system() == 'Windows':
        sh = True
        polycmd = 'gdal_polygonize'
    else:
        sys.exit("Platform not supported!")

    seg_input = seg_fld + '/seg_input.tif'
    seg_output_pre = seg_fld + '/seg_pre.tif'
    seg_mask = seg_fld + '/seg_mask.tif'

    if not os.path.exists(seg_fld):
        os.mkdir(seg_fld)

    extent_buf = seg_fld + '/' + os.path.splitext(os.path.basename(extent))[0] + '_inner.shp'

    # Apply negative buffer to extent to avoid out-of-extent location during feature extraction
    buffer(extent, extent_buf, -150)

    cmd = ['gdalwarp', '-q', '-of', 'GTiff', '-ot', 'UInt16', '-dstnodata', '0', '-srcnodata', '0', '-cutline',
           extent_buf, '-crop_to_cutline', seg_src, seg_input]
    subprocess.call(cmd, shell=sh)

    cmd = ['otbcli_LSGRM', '-in', seg_input, '-out', seg_output_pre, 'int32', '-threshold', params[0],
           '-criterion.bs.cw', params[1], '-criterion.bs.sw', params[2], '-tmpdir', seg_fld]
    if params[3].isdigit() and int(params[3]) > 0:
        cmd += ['-memory', params[3]]
    else:
        warnings.warn("Memory limitation parameter is off, zero or incorrect. Skipping.")
    subprocess.call(cmd, shell=sh)

    cmd = ['otbcli_ManageNoData', '-in', seg_input, '-out', seg_mask, 'uint8', '-mode', 'buildmask']
    subprocess.call(cmd, shell=sh)

    cmd = ['otbcli_ManageNoData', '-in', seg_output_pre, '-out', seg_output, 'int32', '-mode', 'apply',
           '-mode.apply.mask', seg_mask]
    subprocess.call(cmd, shell=sh)

    os.remove(seg_output_pre)
    os.remove(seg_mask)
    for fl in glob.glob(os.path.splitext(extent_buf)[0] + '.*'):
       os.remove(fl)

    seg_vec_output = os.path.splitext(seg_output)[0] + '.gml'
    seg_vec_layername = os.path.splitext(os.path.basename(seg_vec_output))[0]
    cmd = [polycmd, seg_output, '-f', 'GML', seg_vec_output, seg_vec_layername, 'Segment_ID']
    # Check!
    if not os.path.exists(seg_vec_output):
        subprocess.call(cmd, shell=sh)

    out_list = []
    seg_ds = gdal.Open(seg_output)
    geoT = seg_ds.GetGeoTransform()
    spx, spy = str(geoT[1]), str(geoT[5])
    resol = min(abs(geoT[1]), abs(geoT[5]))
    seg_ds = None
    # grid fixed to roughly 5000x5000 pixels
    gsize = round(5000 * resol)
    epsg = int(checkSRS(seg_output).split(':')[-1])
    grid_list = generateGridBasedSubsets(seg_vec_output, [gsize, gsize], epsg)


    for fn in grid_list:
        vec_ds = ogr.Open(fn)
        count = vec_ds.GetLayer(0).GetFeatureCount()
        vec_ds = None
        if count > 0:
            cmd = ['otbcli_Rasterization', '-in', fn, '-out', os.path.splitext(fn)[0] + '.tif', '-spx', spx, '-spy', spy,
                   '-mode', 'attribute', '-mode.attribute.field', 'Segment_ID']
            subprocess.call(cmd, shell=sh)
            out_list.append(fn)
        else:
            for fd in glob.glob(os.path.splitext(fn)[0] + '.*'):
                os.remove(fd)

    return out_list

def generateGTSamples(gt_shp, tile_list, out_fld, ath):
    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    toMerge = []

    for tf in tile_list:
        fn = out_fld + '/GT_' + os.path.basename(tf)
        ok = intersectSamples(tf,gt_shp,fn,splitting=True,area_th=ath)
        if ok:
            rasterizeOnReference(fn,'Segment_ID',tf.replace('.shp','.tif'),fn.replace('.shp','.tif'))
            toMerge.append(fn)

    return toMerge

def generateVALSamples(val_shp, tile_list, out_fld):
    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    val_list = []

    for tf in tile_list:
        fn = out_fld + '/VAL_' + os.path.basename(tf)
        ok = selectSamples(tf,val_shp,fn)
        if ok:
            rasterizeOnReference(fn,'Segment_ID',tf.replace('.shp','.tif'),fn.replace('.shp','.tif'))
            val_list.append(fn)

    return val_list
