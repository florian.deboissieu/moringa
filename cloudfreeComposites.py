import getopt
import glob
import os
import platform
import subprocess
import sys
import gdal
from mtdUtils import setNoDataValue

gdt_to_otb = {2 : 'uint16', 3 : 'int16', 6 : 'float'}

def gatherDates(fld,ofld='.'):
    lst = sorted([name for name in os.listdir(fld) if (os.path.isdir(os.path.join(fld, name)) and (name[0:2] in ['S2','L8']))])
    ofn = ofld + '/input_dates.txt'
    sns = set()
    with open(ofn,'w') as fl:
        for l in lst:
            fl.write(l.split('_')[-2] + '\n')
            sns.add(l[0:2])
    if len(sns) != 1 or list(sns)[0] not in ['S2','L8']:
        sys.exit('Error: directory contains images from multiple sensors or invalid detected folders.')
    return lst,list(sns)

def genComposite(fld, od_fn, ofld = '.'):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")
    lst,sns = gatherDates(fld,ofld)
    ptrn = '*STACK_*.tif'
    if sns == 'L8':
        ptrn = '*_PS_TOA_*.tif'
    gptrn = '*GAPMASK*.tif'
    stacklist = []
    gaplist = []
    nc = set()
    dt = set()
    for l in lst:
        fn = glob.glob(fld + '/' + l + '/' + ptrn)[0]
        ds = gdal.Open(fn)
        nc.add(ds.RasterCount)
        dt.add(ds.GetRasterBand(1).DataType)
        ds = None
        stacklist.append(fn)
        fn = glob.glob(fld + '/' + l + '/' + gptrn)[0]
        gaplist.append(fn)

    if len(nc) != 1:
        sys.exit('Error: stacks to composite do not have the same number of bands.')
    if len(dt) != 1:
        sys.exit('Error: stacks to composite do not have the same data format.')

    dtout = gdt_to_otb[list(dt)[0]]
    ncomp = str(list(nc)[0])

    cmd = ['otbcli_ConcatenateImages','-il'] + stacklist + ['-out', ofld + '/TIMESERIES.tif', dtout]
    subprocess.call(cmd,shell=sh)
    setNoDataValue(ofld + '/TIMESERIES.tif', -10000)

    cmd = ['otbcli_ConcatenateImages', '-il'] + gaplist + ['-out', ofld + '/GAPMASKS.tif', 'uint8']
    subprocess.call(cmd, shell=sh)

    cmd = ['otbcli_ImageTimeSeriesGapFilling', '-in', ofld + '/TIMESERIES.tif', '-mask', ofld + '/GAPMASKS.tif', '-out', ofld + '/TIMESERIES_GAPF.tif', dtout, '-comp', ncomp, '-it', 'linear', '-id', ofld + '/input_dates.txt', '-od', od_fn]
    subprocess.call(cmd, shell=sh)
    setNoDataValue(ofld + '/TIMESERIES_GAPF.tif', -10000)

    return