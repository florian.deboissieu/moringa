rem *** Variables to be modified ***
set WGET_BIN=D:\CrossSite\Dev\WGET
set GIT_BIN=C:\Program Files\Git\bin
set OTB_ROOT=D:\CrossSite\Dev\OTB

rem *** DO NOT MODIFY! ***
set OTB_APPLICATION_PATH=%OTB_ROOT%\lib\otb\applications
set PYTHONPATH=%OTB_ROOT%\lib\otb\python;%PYTHONPATH%
set PATH=%PATH%;%OTB_ROOT%\bin;%WGET_BIN%;%GIT_BIN%
set RIOS_DFLT_DRIVER=GTiff