import getopt
import glob
import os
import platform
import stat
import sys


def main(argv):
    try:
        opts, args = getopt.getopt(argv, '', ['cloudmask', 's2ref=', 's2bandlist='])
    except getopt.GetoptError as err:
        print str(err)

    opt_str = ''
    opt_str_s2 = ''
    base_fld = args[0]

    for opt, val in opts:
        if opt == '--cloudmask':
            opt_str += ' --cloudmask'
        elif opt == '--s2ref':
            opt_str_s2 += ' --s2ref ' + val
        elif opt == '--s2bandlist':
            opt_str_s2 += ' --s2bandlist ' + val

    valid_dirs_L8 = []
    valid_dirs_S2 = []
    valid_dirs_S2_THEIA = []
    for d in os.walk(base_fld):
        fld = d[0]
        mdfL8 = glob.glob(fld + '/*_MTL*.txt')
        mdfS2 = glob.glob(fld + '/tileInfo.json')
        mdfS2THEIA = glob.glob(fld + '/*_MTD_ALL.xml')
        if len(mdfL8) > 0:
            valid_dirs_L8.append(fld)
        elif len(mdfS2) > 0:
            valid_dirs_S2.append(fld)
        elif len(mdfS2THEIA) > 0:
            valid_dirs_S2_THEIA.append(fld)

    if (len(valid_dirs_L8) + len(valid_dirs_S2) + len(valid_dirs_S2_THEIA)) == 0:
        sys.exit('No products found within ' + base_fld)

    if platform.system() == 'Windows':
        f = open('ProcessScript.bat', 'w')
        f.write("@echo off\n")
    else:
        f = open('ProcessScript.sh', 'w')

    for d in valid_dirs_L8:
        f.write('python preprocess.py ' + opt_str + ' ' + d + '\n')
    for d in valid_dirs_S2:
        f.write('python preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n')
    for d in valid_dirs_S2_THEIA:
        f.write('python preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n')

    f.close()

    if platform.system() == 'Linux':
        st = os.stat('ProcessScript.sh')
        os.chmod('ProcessScript.sh', st.st_mode | stat.S_IEXEC)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python genProcessScript.py [--cloudmask] [--s2ref <ref_band>] [--s2bandlist <colon separated band list>] <images-dir>')
    main(sys.argv[1:])
