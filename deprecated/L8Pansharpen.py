import getopt
import shutil
import sys
import time

import scipy.misc as scm

from mtdUtils import *


def applyCloudMask(_in, _out, _cmask):
    fn = os.path.basename(_in)
    ds_in = gdal.Open(_in)
    band = ds_in.GetRasterBand(1)
    typ_in = band.DataType
    band_arr = bandToArray(band)
    if os.path.isfile(_cmask):
        ds_mask = gdal.Open(_cmask)
        bandm = ds_mask.GetRasterBand(1)
        cm = bandToArray(bandm)
        if '_P_' in fn or '_PS_' in fn:
            cm = scm.imresize(cm, band_arr.shape, interp='nearest', mode='F')
        msk = np.logical_or(cm == 1, cm == 0)
        # Deallocate
        bandm = None
        ds_mask = None
        cm = None
        # Read image to mask and generate masked output
        ds_out = createOverlappingTIFF(_out, ds_in.RasterCount, typ_in, ds_in)
        msk = np.logical_and(msk, band_arr > 0)
        band = None
        band_arr = None
        for k in range(0, ds_in.RasterCount):
            band = ds_in.GetRasterBand(k + 1)
            band_out = ds_out.GetRasterBand(k + 1)
            band_arr = bandToArray(band)
            band_masked = np.multiply(band_arr, msk)
            arrayToBand(band_out, band_masked)
            band_out.FlushCache()
            # Deallocate local
            band = None
            band_out = None
            # Deallocate
        ds_in = None
        ds_out = None
        return 0
    else:
        print('Cloud mask file not found!')
        return -1


def L8_sharpen(pf, mf, in_fld, out_fld, msk):
    # PAN (just copy)
    panfile_out = out_fld + os.path.basename(pf)
    if not os.path.isfile(panfile_out):
        shutil.copyfile(pf, panfile_out)
    else:
        print 'PAN file already exists in output folder!'
        # MS (layer stack)
    msfile_out = out_fld + os.path.basename(mf)
    if not os.path.isfile(msfile_out):
        shutil.copyfile(mf, msfile_out)
    else:
        print 'MS file already exists in output folder!'
        # MS_UP file (temporary)
    msupfile = mf.replace('MS', 'MS_UP')
    msupfile_out = out_fld + os.path.basename(msupfile)
    # PS (bayes pansharpening)
    psfile = mf.replace('MS', 'PS')
    psfile_out = out_fld + os.path.basename(psfile)
    if not os.path.isfile(psfile_out):
        subprocess.call(
            'otbcli_Superimpose.bat -inr \"' + panfile_out + '\" -inm \"' + msfile_out + '\" -out \"' + msupfile_out + '\" uint16')
        subprocess.call(
            'otbcli_Pansharpening.bat -inp \"' + panfile_out + '\" -inxs \"' + msupfile_out + '\" -out \"' + psfile_out + '\" uint16 -method bayes')
        setNoDataValue(psfile_out)
    else:
        print 'PS file already exists in output folder!'
        # Apply cloud mask
    if msk == True:
        # Load cloud mask
        cmask = mf.replace('_OLI_MS_TOARef_', '_cfmask_')
        ext = os.path.splitext(msfile_out)[-1]
        _out = msfile_out.replace(ext, '_noclouds' + ext)
        if not os.path.isfile(_out):
            applyCloudMask(msfile_out, _out, cmask)
            setNoDataValue(_out)
        else:
            print 'MS noclouds file already exists!'
        ext = os.path.splitext(panfile_out)[-1]
        _out = panfile_out.replace(ext, '_noclouds' + ext)
        if not os.path.isfile(_out):
            applyCloudMask(panfile_out, _out, cmask)
            setNoDataValue(_out)
        else:
            print 'PAN noclouds file already exists!'
        ext = os.path.splitext(psfile_out)[-1]
        _out = psfile_out.replace(ext, '_noclouds' + ext)
        if not os.path.isfile(_out):
            applyCloudMask(psfile_out, _out, cmask)
            setNoDataValue(_out)
        else:
            print 'PS noclouds file already exists!'


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'i:o:m', ['input-folder=', 'output-folder=', 'mask-clouds'])
    except getopt.GetoptError as err:
        print str(err)

    in_folder = os.getcwd() + '/'
    out_folder = './'
    maskClouds = False

    for opt, val in opts:
        if opt in ('-i', '--input--folder'):
            in_folder = val + "/"
        elif opt in ('-o', '--output-folder'):
            out_folder = val + "/"
        elif opt in ('-m', '--mask-clouds'):
            maskClouds = True

    try:
        os.stat(out_folder)
    except:
        os.mkdir(out_folder)

    fn = None
    if len(args) > 0:
        msfile = None
        panfile = None
        for file in os.listdir(in_folder):
            if file.startswith(args[0]) and os.path.splitext(file)[-1].upper() == '.TIF':
                if '_MS_' in file:
                    msfile = in_folder + file
                elif '_P_' in file:
                    panfile = in_folder + file
        if msfile is None or panfile is None:
            sys.exit('MS and/or Pan source not found!')
        else:
            startTime = time.time()
            print "Working on " + args[0] + "..."
            L8_sharpen(panfile, msfile, in_folder, out_folder, maskClouds)
            print('Completed in %s seconds.') % (time.time() - startTime)
    else:
        # Seek for all L8 identifiers in folder
        # Watch out! One image for each identifier allowed per folder!
        idlist = []
        for file in os.listdir(in_folder):
            if '_P_' in file and os.path.splitext(file)[-1].upper() == '.TIF':
                idlist.append(file.split('_')[0])
                # Run for each id
        for id in idlist:
            msfile = None
            panfile = None
            for file in os.listdir(in_folder):
                if file.startswith(id) and os.path.splitext(file)[-1].upper() == '.TIF':
                    if '_MS_' in file:
                        msfile = in_folder + file
                    elif '_P_' in file:
                        panfile = in_folder + file
            if msfile is None or panfile is None:
                sys.exit('MS and/or Pan source for id ' + id + ' not found!')
            else:
                startTime = time.time()
                print "Working on " + id + "..."
                L8_sharpen(panfile, msfile, in_folder, out_folder, maskClouds)
                print('Completed in %s seconds.') % (time.time() - startTime)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit('Usage: python L8_TOARef.py -i <input dir> -o <output dir> [-m]')
    else:
        main(sys.argv[1:])
