import os
import subprocess
import sys


def getSizeFromOTBInfo(str):
    str_lines = str.split('\n')
    line = None
    W, H = -1, -1
    for x in str_lines:
        if 'Size :' in x:
            line = x
    if line is not None:
        line = line[line.index('[') + 1:line.index(']')].split(',')
        W, H = int(line[0]), int(line[1])

    return W, H


def main(argv):
    # Parse args
    wd = argv[0]
    ref = argv[1]
    blist = argv[2].split(';')

    files = os.listdir(wd)
    blist_files = []
    blist_files_todo = []
    for file in files:
        if file.startswith(ref) and file.endswith('.tif'):
            ref_file = file
        elif file[0:3] in blist and file.endswith('.tif'):
            blist_files.append(file)

    ref_info = subprocess.check_output('otbcli_ReadImageInfo.bat -in \"' + wd + '/' + ref_file)
    W, H = getSizeFromOTBInfo(ref_info)
    for bf in blist_files:
        img_info = subprocess.check_output('otbcli_ReadImageInfo.bat -in \"' + wd + '/' + bf)
        w, h = getSizeFromOTBInfo(img_info)
        if w != W or h != H:
            blist_files_todo.append(wd + '/ref_' + bf)
            ss_cmd = 'otbcli_Superimpose.bat -inr \"' + wd + '/' + ref_file + '\" -inm \"' + wd + '/' + bf + '\" -interpolator nn -out \"' + wd + '/' + 'ref_' + bf + '\" uint16 -ram 128'
            subprocess.call(ss_cmd)
        else:
            blist_files_todo.append(wd + '/' + bf)

    blist_files_todo = ['\"' + x + '\"' for x in blist_files_todo]
    blist_files_todo.insert(blist.index(ref), '\"' + wd + '/' + ref_file + '\"')
    print blist_files_todo

    cc_cmd = 'otbcli_ConcatenateImages.bat -il ' + ' '.join(blist_files_todo) + ' -out \"' + wd + '/stack' + ref_file[
                                                                                                             3:] + '\" uint16'
    subprocess.call(cc_cmd)

    blist_files = [x[1:-1] for x in blist_files]
    for bf in blist_files:
        if '/ref_' in bf:
            os.remove(bf)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit('Usage: python S2ResampleAndStack.py <image dir> <ref_band> <semi-colon separated band list>')
    else:
        main(sys.argv[1:])
