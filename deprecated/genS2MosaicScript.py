import getopt
import os
import sys


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'd:g:')
    except getopt.GetoptError as err:
        print str(err)

    bd = os.getcwd()
    gdaldata = None

    # Parse options
    for opt, val in opts:
        if opt == '-d':
            bd = val

    f = open('S2MosaicScript.bat', 'w')
    f.write('@echo off\n')
    f.write('set CURDIR=%~dp0\n')
    # f.write('set ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=2\n')
    f.write('cd \"' + bd + '\"\n')
    f.write('if not exist \"' + bd + '/Mosaics\" md \"' + bd + '/Mosaics\"\n')
    # date_tile
    tiles = args[0].split(';')
    tiles = ['S2_' + x for x in tiles]
    od = tiles[0][0:11]
    f.write('if not exist \"' + bd + '/Mosaics/' + od + '\" md \"' + bd + '/Mosaics/' + od + '\"\n')
    fns = ['B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B09', 'B10', 'B11', 'B12']
    for fn in fns:
        mergelist = ''
        for tile in tiles:
            files = os.listdir(bd + '/' + tile)
            fnb = os.path.splitext(files[0])[0][3:]
            for file in files:
                if file.startswith(fn) and file.endswith('.tif'):
                    mergelist = mergelist + ' \"' + bd + '/' + tile + '/' + file + '\"'
        merge_cmd = 'call otbcli_Mosaic -progress false -il ' + mergelist + ' -out \"' + bd + '/Mosaics/' + od + '/' + fn + fnb + '_mosaic.tif\" uint16'
        f.write(merge_cmd + '\n')
    f.write('cd %CURDIR%')
    f.close()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit('Usage: python genS2MosaicScript.py [-d <base-dir>] <semi-colon separated date_tile identifier>')
    main(sys.argv[1:])
