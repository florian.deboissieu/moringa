import getopt
import os
import subprocess
import sys
from shutil import copyfile


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'm:e:f:x:',
                                   ['mask=', 'extent=', 'output-folder=', 'extension=', 'metadata-ext='])
    except getopt.GetoptError as err:
        print str(err)

    out_folder = './'
    ext = '.TIF'
    meta_ext = None

    for opt, val in opts:
        if opt in ('-m', '--mask'):
            mfull = val
            mname = os.path.split(val)[-1]
            clip_by_mask = True
        elif opt in ('-e', '--extent'):
            clip_by_mask = False
        elif opt in ('-f', '--output-folder'):
            out_folder = val + "/"
        elif opt in ('-x', '--extension'):
            ext = '.' + val.upper()
        elif opt == '--metadata-ext':
            meta_ext = '.' + val

    for r, d, f in os.walk(args[0]):
        for n in f:
            fnamefull = os.path.join(r, n)
            fname = os.path.basename(fnamefull)
            foutfull = out_folder + os.path.splitext(fname)[0] + '_' + os.path.splitext(os.path.basename(mfull))[
                0] + '.tif'
            if os.path.splitext(fname)[-1].upper() == ext:
                print 'Clipping image : ' + fname + '...'
                if clip_by_mask == True:
                    # processing.runalg('gdalogr:cliprasterbymasklayer',fnamefull,mfull,"-9999",False,False,"",out_folder + os.path.splitext(fname)[0] + '_' + os.path.splitext(os.path.basename(mfull))[0] + '.tif')
                    subprocess.call(
                        'gdalwarp -q -of GTiff -dstnodata 0 -cutline \"' + mfull + '\" -crop_to_cutline \"' + fnamefull + '\" \"' + foutfull + '\"')
                else:
                    print 'Not implemented yet'
            elif meta_ext != None and os.path.splitext(fname)[-1].upper() == meta_ext.upper():
                meta_ext = os.path.splitext(fname)[-1]
                # Keep original metadata file, just copy (to be otpimized)
                copyfile(fnamefull,
                         out_folder + os.path.splitext(fname)[0] + '_' + os.path.splitext(os.path.basename(mfull))[
                             0] + meta_ext)


if __name__ == '__main__':
    # execfile('_python_qgis_startup.py')
    main(sys.argv[1:])
