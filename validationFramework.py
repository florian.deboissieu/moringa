import ogr
import os
import numpy as np
import math
import subprocess
import platform
from sklearn.metrics import confusion_matrix, accuracy_score, cohen_kappa_score, precision_recall_fscore_support

def pixelValidation(ref_shp,val_shp,ref_img,txt_out,cfield,pfield=None):
    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    if pfield is None:
        pfield = 'p' + cfield

    tmp = os.path.splitext(val_shp)[0] + '.tif'
    cmd = ['otbcli_Rasterization', '-in', val_shp, '-im', ref_img, '-out', tmp, 'uint16', '-mode', 'attribute', '-mode.attribute.field', pfield]
    subprocess.call(cmd, shell=sh)

    tmp_cm = os.path.splitext(val_shp)[0] + '.confmat.csv'
    cmd = ['otbcli_ComputeConfusionMatrix', '-in', tmp, '-out', tmp_cm, '-ref', 'vector', '-ref.vector.in', ref_shp, '-ref.vector.field', cfield]
    res = subprocess.check_output(cmd,shell=sh)

    os.remove(tmp)

    tid = open(txt_out, "w")
    tid.write(res)
    tid.close()


def surfaceValidation(ref_shp,val_shp,out,cfield,pfield=None):
    if pfield is None:
        pfield = 'p' + cfield

    shpd = ogr.GetDriverByName('ESRI Shapefile')
    ref_ds = ogr.Open(ref_shp, 0)
    val_ds = ogr.Open(val_shp, 0)
    ref_ly = ref_ds.GetLayer(0)
    val_ly = val_ds.GetLayer(0)

    out_ds = shpd.CreateDataSource(out)
    out_ly = out_ds.CreateLayer(os.path.splitext(os.path.basename(out))[0],
                             srs=val_ly.GetSpatialRef(),
                             geom_type=val_ly.GetLayerDefn().GetGeomType())

    feat = val_ly.GetFeature(0)
    cidx = feat.GetFieldIndex(cfield)
    pidx = feat.GetFieldIndex(pfield)
    out_ly.CreateField(feat.GetFieldDefnRef(cidx))
    out_ly.CreateField(feat.GetFieldDefnRef(pidx))
    err_fld = ogr.FieldDefn("ERR", ogr.OFTInteger)
    out_ly.CreateField(err_fld)

    y_true = []
    y_pred = []
    y_wght = []

    for rf in ref_ly:
        rg = rf.GetGeometryRef()
        val_ly.SetSpatialFilter(rg)
        val_ly.ResetReading()
        for vf in val_ly:
            vg = vf.GetGeometryRef()
            og = vg.Intersection(rg)
            if og is not None and og.GetArea() > 0:
                of = ogr.Feature(out_ly.GetLayerDefn())
                of.SetGeometry(og)
                c = vf.GetField(cidx)
                p = vf.GetField(pidx)
                e = int(c != p)
                of.SetField(0, c)
                of.SetField(1, p)
                of.SetField(2, e)
                out_ly.CreateFeature(of)
                y_true.append(int(c))
                y_pred.append(int(p))
                y_wght.append(og.GetArea())
            elif og is None:
                print vg.GetArea()

    ref_ds = None
    val_ds = None
    out_ds = None

    cm = confusion_matrix(y_true,y_pred,sample_weight=y_wght)
    acc = accuracy_score(y_true, y_pred, sample_weight=y_wght)
    kappa = cohen_kappa_score(y_true, y_pred, sample_weight=y_wght)
    prf = precision_recall_fscore_support(y_true, y_pred, sample_weight=y_wght)
    classes = sorted(np.unique(y_true))

    return classes,cm,acc,kappa,prf


def formatValidationTxt(classes,cm,acc,kappa,prf,txt_out):
    # Format text output
    tid = open(txt_out, "w")
    tid.write('Confusion matrix (surfaces):\n\n')
    ndigit = int(math.ceil(math.log10(np.max(cm))))
    cm_fmt = '%' + str(ndigit + 3) + '.2f'
    hd_fmt = '[ %' + str(ndigit - 1) + 'd ]'
    tid.write(' ' * (ndigit + 3) + ' ' + ' '.join([hd_fmt % x for x in classes]) + '\n\n')
    i = 0
    for l in cm:
        tid.write(hd_fmt % classes[i] + ' ' + ' '.join([cm_fmt % x for x in l]) + '\n')
        i += 1

    tid.write('\n')

    pr_fmt = ' ' * (ndigit - 3) + '%6.4f'
    tid.write('Per-class figures :\n\n')
    tid.write(' ' * (ndigit + 3) + ' ' + ' '.join([hd_fmt % x for x in classes]) + '\n\n')
    tid.write('PREC' + ' ' * (ndigit + 3 - 4) + ' ' + ' '.join([pr_fmt % x for x in prf[0]]) + '\n')
    tid.write('RECALL' + ' ' * (ndigit + 3 - 6) + ' ' + ' '.join([pr_fmt % x for x in prf[1]]) + '\n')
    tid.write('F-MEAS' + ' ' * (ndigit + 3 - 6) + ' ' + ' '.join([pr_fmt % x for x in prf[2]]) + '\n')

    tid.write('\n')

    tid.write('Overall Accuracy: %5.2f%%\n' % (acc * 100))
    tid.write('Kappa           : %6.4f\n' % (kappa))

    tid.close()